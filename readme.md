# Semantic Similarity

This repository would contain implementation of semantic similarity with different state of the art transformers and benchmark the performance for the same with the [SNLI corpus](https://nlp.stanford.edu/projects/snli/) of Stanford.

## Introduction

Semantic similarity is used to measure the distance or similarity between two pair of words, phrases, sentences or documents. This can be done in two ways knowledge-based or corpus-based. Here we would be doing a corpus-bases approach. This implementation is inspired from semantic similarity implementation with BERT example from [keras.io](https://keras.io/examples/nlp/semantic_similarity_with_bert/). The idea here is to change the models and see if we could get a better performance with other transformer architecture like t5.

## Requirements
- Python 3.6 +
- Huggingface
- Pytorch 1.0.0
- AllenNLP 0.8.1

## How to Contribute

If you feel that there can be a better model that can be used to do semantic-similarity, implement the model in a notebook validate the result and include the screenshot in the Pull Request. If you have the model as a python project make a separate module like [SemBERT](https://gitlab.com/abhijitramesh/semantic-similarity/-/tree/master/SemBERT) and include instruction in readme for execution and validation of the results.

- Please go through the [Code of Conduct](https://gitlab.com/abhijitramesh/semantic-similarity/-/blob/master/CODE_OF_CONDUCT.md)
- Read from the [Contribution Guidelines](https://gitlab.com/abhijitramesh/semantic-similarity/-/blob/master/CONTRIBUTING.md) as well.
- If you find any improvements to the current implementation or any bugs please feel free to open an issue.

## Project Structure
- [BERT-base-uncased notebook](https://gitlab.com/abhijitramesh/semantic-similarity/-/blob/master/Sentence_Similarity_using_BERT.ipynb) : This notebook has implementation, training and evaluation of both with and without fine-turning on BERT

- [T5-small notebook](https://gitlab.com/abhijitramesh/semantic-similarity/-/blob/master/Sentence_Similarity_using_t5small.ipynb) :  This notebook has implementation,training and evaluation of both with and without fine-tuning for T5-small


- [T5-base notebook](https://gitlab.com/abhijitramesh/semantic-similarity/-/blob/master/Sentence_Similarity_using_T5base.ipynb) : This notebook has implementation,training and evaluation of both with and without fine-tuning for T5-base

- [SemBERT](https://gitlab.com/abhijitramesh/semantic-similarity/-/tree/master/SemBERT) : This folder contains the modules on training and evaluating the SemBERT model on SNLI with weights for pretrained BERT as well as Semantic Role Labeler.

## License
This project is released under a free and open-source software license, Apache 2.0 or later ([LICENSE](https://gitlab.com/abhijitramesh/semantic-similarity/-/blob/master/LICENSE) or [LICENSE 2.0](https://www.apache.org/licenses/LICENSE-2.0) )

## Resources
- [Semantic-aware BERT for Language Understanding](https://arxiv.org/abs/1909.02209)
- [BERT](https://arxiv.org/pdf/1810.04805.pdf)
- [T5](https://arxiv.org/abs/1910.10683)
- [SNLI](https://nlp.stanford.edu/projects/snli/)

## Citation
```
@article{2020t5,
  author  = {Colin Raffel and Noam Shazeer and Adam Roberts and Katherine Lee and Sharan Narang and Michael Matena and Yanqi Zhou and Wei Li and Peter J. Liu},
  title   = {Exploring the Limits of Transfer Learning with a Unified Text-to-Text Transformer},
  journal = {Journal of Machine Learning Research},
  year    = {2020},
  volume  = {21},
  number  = {140},
  pages   = {1-67},
  url     = {http://jmlr.org/papers/v21/20-074.html}
}
```
```
@article{devlin2018bert,
  title={BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding},
  author={Devlin, Jacob and Chang, Ming-Wei and Lee, Kenton and Toutanova, Kristina},
  journal={arXiv preprint arXiv:1810.04805},
  year={2018}
}
```
```
@inproceedings{zhang2020SemBERT,
	title={Semantics-aware {BERT} for language understanding},
	author={Zhang, Zhuosheng and Wu, Yuwei and Zhao, Hai and Li, Zuchao and Zhang, Shuailiang and Zhou, Xi and Zhou, Xiang},
  	booktitle={the Thirty-Fourth AAAI Conference on Artificial Intelligence (AAAI-2020)},
	year={2020}
}
```
