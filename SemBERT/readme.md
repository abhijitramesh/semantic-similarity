# Introduction
Semantic similarity is the task of measuring the distance or similarity between two pairs of words, phrases, sentences or documents. Semantic similarity comes under one of the flagship natural language understanding (NLU) tasks. Deep contextual models have shown to give state-of-the-art results in these tasks. Models such as ELMo, GPT, BERT and XLNet are some prominent examples. The authors argue that even though these models provide such good performances, they can be improved by incorporating external knowledge for representing contextual semantics compared to the existing techniques where these representations are implicitly learned by the pre-trained models from the plain contextual features for representation as well as training objectives. Significant amounts of [studies](https://arxiv.org/abs/1805.05492) have shown that deep learning models are very prone to adversarial attacks since they do not really understand the natural language texts and pay more attention to non-significant words while ignoring the important ones. in NLU tasks such as question answering which shows that the models are suffering from insufficient contextual semantic representation and learning. The authors propose adding external knowledge to these pre-trained language models. Why this would work is because generally for a question answer task like who did what to whom, when and why seems answerable to us humans because we have contextual knowledge and a sentence in human conversation involves a predicate-argument structure while for a neural network this is embedding representation with little consideration of the modeling of multiple semantic structures. This is exactly what SemBERT is. SemBERT is fine-tuned BERT with contextual semantic cues which learn these representations in a fine-grained manner by taking advantage of BERT’s ability on plain context representation and explicit semantics for deeper meaning representation

# Sematic-aware BERT
<img width="584" alt="Screenshot 2021-04-14 at 2 22 36 PM" src="https://user-images.githubusercontent.com/43090559/114682592-ddc92180-9d2c-11eb-9d29-29f0c9d9f918.png">

Figure 1: SemBERT. * pre-trained labeller (not fine tuned in this framework)

Figure 1 above shows the representation of SemBERT which has some modification over the BERT architecture [2]. Multiple predicate-derived structures of the explicit semantics of the words that are input to SemBERT are fetched by passing it to a semantic role labeler the corresponding embeddings are aggregated after a linear layer to form the final semantic embedding. In addition to this parallel the input sequences are segmented into subwords by BERT word-piece tokenizer. In order to get back the contextual word representation the subwords are transformed back via convolutional layers. Finally the word representation and semantic embeddings are combined to form the join representation for downstream tasks.

#### Semantic Role Labeling
For preprocessing the data, each sentence is annotated into several semantic sequences using a pre-trained semantic role labeler. This is done by [PropBank Style](https://www.aclweb.org/anthology/D09-1004.pdf) of semantic role to annotate every token of input sequence with semantic labels. For every sentence there would be various predicate-argument structures. 

#### Encoding
To the pretrained BERT model we first pass in the raw text sequence encoding as well as the semantic role label sequences initially as embedding vectors. The input sentence X = {x1, . . . , xn} is the sequence of words of length n, which is first tokenized to subword tokens. Using self-attention the transformer’s encoder captures the contextual information for each token. For m label sequences related to each predicate, there is T = {t1,.......,™} where ti contains n labels noted as {labeli 1 , labeli 2 , ..., labeli n}. Each of the semantic signals are considered as embeddings and we use a lookup table to map these labels to vectors s {v i 1 , vi 2 , ..., vi n} which is then fed into a BiGRU layer to obtain the label representations for m label sequence in latent space, e(ti) = BiGRU(v i 1 , vi 2 , . . . , vi n ) where 0 < i<= m. let L denote the label sequence for token xi, we have e(Li) = {e(t1), . . . , e(tm)}. In order to obtain the joint representation we concatenate the m label sequences, of label representation and feed them to a fully connected layer. The refined joint representation e t in dimension d:

<img width="396" alt="Screenshot 2021-04-14 at 2 27 38 PM" src="https://user-images.githubusercontent.com/43090559/114683346-92634300-9d2d-11eb-8abb-c1b54b487a15.png">

Where W2 and b2 are trainable parameters.

#### 2.3 Integration
The pretrained bert model that is used here is based on subwords whereas the semantic labels that are introduced here are based on words. These different size sequences need to be aligned; that is where the interaction model comes in which fuses the lexical text embedding and label representation. In order to get the word-level representation the subwords are grouped and passed through a convolutional neural network with max pooling. For any given word xi is made up of a sequence of subwords [s1,s2,....,sl] where l is the number of subwords in the word x.  Denoting the representation of subword sj from BERT as e(sj ), first a Conv1D layer is utilised, e’i = W1 [e(si), e(si+1), . . . , e(si+k−1)] + b1, where W1 and b1 are trainable parameters and k is the kernel size. On the output embedding sequence for xi we apply ReLU :
The whole representation for words sequence X is given by  ew = {e(x1), . . . e(xn)} ∈ Rn x dw where dw is the dimension of word embedding.
The aligned context and distilled semantic embeddings are then merged by a fusion function h = ew ◇ et , where ◇ represents concatenation operation.
# Model Implmentation
SemBERT can be used as a forepart encoder for a wide range of tasks and could also become an end-to-end model with one linear layer for doing prediction tasks. For ease of reproduction I have only used the straightforward SemBERT that directly gives the prediction after fine-tuning.

### Semantic Role Labeler
The semantic role labels are obtained by using a pretrained SRL module which predicts all the predicates and corresponding arguments in one shot. This labeler is implemented from [4] which achieves an F! Of 84.6% on English OntoNotes v5.0 benchmark dataset for the CoNNL-2012 shared task. For this implementation there are 104 labels in total. O is used for non-argument words and Verbe label for predicates.

### Task-specific Fine--tuning
For the adaption to do classification, regression and span-based MRC tasks. In order to obtain the prediction distribution the fused contextual semantic and LRM representation h is transformed to a lower dimension. This is the same as the implementation of BERT without any modification so that no extra influences and focus on the intrinsic performance of SemBERT is made. For classification and regression tasks, h is passed to the fully connected layer to get the class logits or scores respectively. The objectives are CrossEntropy for classification and Mean Square Error for regression tasks. Inorder to do span-based reading comprehension, h is passed to fully connected layer to get start logits s as well as end logits e for all the tokens. The score of a candidate span from position i to positation j is given as si + ej. The maximum scoring span where j ≥ i is used as prediction. We compare the score of the pooled first token span snull = s0 + e0 to the score of the best non-null span sˆi,j = maxj<=i(si + ej ). We predict a non-null answer when sˆi,j > snull+τ , where the threshold τ is selected on the dev set to maximize F1.

# Requirements

- Python 3.6+ 

- PyTorch 1.0.0

- AllenNLP 0.8.1

# Dataset


GLUE data can be downloaded from [GLUE data](https://gluebenchmark.com/tasks) by running [this script](https://gist.github.com/W4ngatang/60c2bdb54d156a41194446737ce03e2e) and unpack it to directory glue_data. We provide an example data sample in glue_data/MNLI to show how SemBERT works.

## Instructions
This repo shows the example implementation of SemBERT for NLU tasks.
We basically used the pre-trained BERT uncased models so do not forget to pass the parameter `--do_lower_case`.

The example script are as follows:

**Train a model**

Note: please replace the sample data with labeled data (use the labeled data or annotate your data following the instructions below).

```shell
CUDA_VISIBLE_DEVICES=0 \
python run_classifier.py \
--data_dir glue_data/SNLI/ \
--task_name snli \
--train_batch_size 32 \
--max_seq_length 128 \
--bert_model bert-wwm-uncased \
--learning_rate 2e-5 \
--num_train_epochs 2 \
--do_train \
--do_eval \
--do_lower_case \
--max_num_aspect 3 \
--output_dir glue/snli_model_dir
```

**Evaluation**

Both `run_classifier.py ` and  `run_snli_predict.py` can be used for evaluation, where the later is simplified for easy employment.

The major difference is that `run_classifier.py` takes labeled data as input, while `run_snli_predict.py` integrates the real-time semantic role labeling, so it uses the original raw data.

**Evaluation using labeled data**

```shell
CUDA_VISIBLE_DEVICES=0 \
python run_classifier.py \
--data_dir glue_data/SNLI/ \
--task_name snli \
--eval_batch_size 128 \
--max_seq_length 128 \
--bert_model bert-wwm-uncased \
--do_eval \
--do_lower_case \
--max_num_aspect 3 \
--output_dir glue/snli_model_dir
```

**Evaluation using raw data (with real-time semantic role labeling)** 

trained weights SNLI model (reaching 91.9% test accuracy) can be accessed [here](https://drive.google.com/drive/folders/1Yn-WCw1RaMxbDDNZRnoJCIGxMSAOu20_?usp=sharing).


To use the trained SNLI model, please put the [SNLI model](https://drive.google.com/open?id=1Yn-WCw1RaMxbDDNZRnoJCIGxMSAOu20_) and the [SRL model](https://s3-us-west-2.amazonaws.com/allennlp/models/srl-model-2018.05.25.tar.gz) to the **snli_model_dir** and **srl_model_dir**, respectively.

As shown in the example SNLI model, the folder of **snli_model_dir** should contain three files:

*vocab.txt* and *bert_config.json* from the BERT model folder that are used for training your model;

*pytorch_model.bin* that is the trained SNLI model.

```shell
CUDA_VISIBLE_DEVICES=0 \
python run_snli_predict.py \
--data_dir /share03/zhangzs/glue_data/SNLI \
--task_name snli \
--eval_batch_size 128 \
--max_seq_length 128 \
--max_num_aspect 3 \
--do_eval \
--do_lower_case \
--bert_model snli_model_dir \
--output_dir snli_model_dir \
--tagger_path srl_model_dir
```

For prediction, use the flag: `--do_predict` for either the script `run_classifier.py` or `run_snli_predict.py`. The output pred file can be directly used for GLUE online submission and evaluation.

### Data annotation (Semantic role labeling)

We provide two kinds of semantic labeling method, 

* **online**: each word sequence are passed to label module to obtain the tags which could be used for online prediction. This would be time-consuming for large corpus. See  *tag_model/tagging.py*

  If you want to use the online one, please specify the `--tagger_path` parameter in the run.py file.

* **offline**: the current one that pre-process the datasets and save them for later loading for training and evaluation. See *tag_model/tagger_offline.py*

The labeled data can be downloaded [here](https://drive.google.com/file/d/1B-_IRWRvR67eLdvT6bM0b2OiyvySkO-x/view?usp=sharing) for quick start.

  

Note this repo is based on the offline version, so that the column id/index in the data-processor would be slightly different from the original, which is like this:

text_a = line[-3]
text_b = line[-2]
label = line[-1]

If you use the original data <u>instead of</u> the preprocessed one by tag_model/tagger_offline.py, please modify the index according to the dataset structure.

### SRL model

The SRL model in this implementation used the [ELMo-based SRL model](https://s3-us-west-2.amazonaws.com/allennlp/models/srl-model-2018.05.25.tar.gz)  from [AllenNLP](https://github.com/allenai/allennlp). 

Recently, there is a new [BERT-based model](https://s3-us-west-2.amazonaws.com/allennlp/models/bert-base-srl-2019.06.17.tar.gz), which is a nice alternative. 

### Reference


```
@inproceedings{zhang2020SemBERT,
	title={Semantics-aware {BERT} for language understanding},
	author={Zhang, Zhuosheng and Wu, Yuwei and Zhao, Hai and Li, Zuchao and Zhang, Shuailiang and Zhou, Xi and Zhou, Xiang},
  	booktitle={the Thirty-Fourth AAAI Conference on Artificial Intelligence (AAAI-2020)},
	year={2020}
}
```